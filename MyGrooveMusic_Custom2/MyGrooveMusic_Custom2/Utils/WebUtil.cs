﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace MyGrooveMusic.Utils
{
    public static class WebUtil
    {
        public static async Task<Stream> GetRequestStreamAsync(HttpWebRequest webRequest)
        {
            Stream stream = null;
            IAsyncResult result = webRequest.BeginGetRequestStream((asyncResult) =>
            {
                stream = webRequest.EndGetRequestStream(asyncResult);
            }, null);

            while (!result.IsCompleted)
            {
                await Task.Delay(50);
            }

            return stream;
        }

        public static async Task<WebResponse> GetResponseAsync(HttpWebRequest webRequest)
        {
            WebResponse response = null;
            IAsyncResult result = webRequest.BeginGetResponse((asyncResult) =>
            {
                response = webRequest.EndGetResponse(asyncResult);

            }, null);

            while (!result.IsCompleted)
            {
                await Task.Delay(50);
            }

            return response;
        }
    }
}
