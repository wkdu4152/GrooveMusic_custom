﻿using MyGrooveMusic.JsonConverters;
using MyGrooveMusic.source.container;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MyGrooveMusic.Utils
{
    public static class CommonUtil
    {
        private static readonly string[] POSSIBLE_PLAY_MEDIA_TYPS = new string[] { ".mp3", ".flac", ".wav" };

        public static bool IsCanPlayFile(string fileName)
        {
            int extendIndex = fileName.LastIndexOf('.');
            if (extendIndex > 0 && (fileName.Length - extendIndex) >= 0)
            {
                string extendValue = fileName.Substring(extendIndex, fileName.Length - extendIndex);

                return POSSIBLE_PLAY_MEDIA_TYPS.Contains<string>(extendValue);
            }

            return false;
        }
        public static List<OneDriveInfoContainer> GetParsedOndriveItems(string readData)
        {
            List<OneDriveInfoContainer> fileDatas = null;

            Dictionary<string, object> recivedData = JsonConvert.DeserializeObject<Dictionary<string, object>>(readData);

            JContainer values = recivedData["value"] as JContainer;
            if (values != null)
            {
                IEnumerator itor = values.Children().GetEnumerator();
                fileDatas = new List<OneDriveInfoContainer>();
                OneDriveInfoContainer container = null;
                JContainer jsonValue = null;

                while (itor.MoveNext())
                {
                    jsonValue = itor.Current as JContainer;
                    if (jsonValue != null)
                    {
                        JsonSerializerSettings setting = new JsonSerializerSettings();
                        setting.Converters.Add(new OnedriveInfoJsonConverter());
                        container = JsonConvert.DeserializeObject<OneDriveInfoContainer>(jsonValue.ToString(), setting);
                        if (jsonValue["@content.downloadUrl"] != null)
                        {
                            container.DownloadUrl = (string)jsonValue["@content.downloadUrl"];
                        }
                        fileDatas.Add(container);

                        container = null;
                    }
                    jsonValue = null;
                }
            }
            if (recivedData != null)
            {
                recivedData.Clear();
            }

            return fileDatas;
        }

        public static long? GetCRCValue(byte[] bytes)
        {
            if(bytes == null)
            {
                return null;
            }

            const int crcLength = 20;
            byte[] crc = new byte[crcLength];

            for (int loops = 0; loops < bytes.Length;)
            {
                int crcIndex = 0;
                for (int index = loops; index < loops + crcLength; index++)
                {
                    if (index < bytes.Length)
                    {
                        if (crc[crcIndex] == 0)
                        {
                            crc[crcIndex] = (byte)(0xff & bytes[index]);
                        }
                        else
                        {
                            crc[crcIndex] = (byte)(crc[crcIndex] & bytes[index]);
                        }
                    }
                    crcIndex++;
                }
                loops += crcLength;
            }

            return BitConverter.ToInt64(crc, 0);
        }
    }
}
