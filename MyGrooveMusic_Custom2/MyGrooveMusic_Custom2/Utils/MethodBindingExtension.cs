﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using Expression = System.Linq.Expressions.Expression;


namespace MyGrooveMusic_Custom2.Utils
{
    public delegate void MethodProviderHandler(params object[] parameters);

    public class MethodBindingExtension : MarkupExtension
    {
        private class CommandProvider : ICommand
        {
            private readonly MethodBindingProvider _provider;
            public CommandProvider(MethodBindingProvider provider) { this._provider = provider; }
            public void Execute(object parameter) { _provider.MethodProviderHandler(parameter); }
            public bool CanExecute(object parameter) { return true; }
            public event EventHandler CanExecuteChanged;
        }

        private readonly string _method;

        public MethodBindingExtension(string method)
        {
            this._method = method;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            IProvideValueTarget provideValueTarget = serviceProvider as IProvideValueTarget;
            if (provideValueTarget == null || this._method == null) return null;

            MethodBindingProvider provider = new MethodBindingProvider(provideValueTarget.TargetObject as FrameworkElement, this._method);

            Type memberType = GetMemberType(provideValueTarget.TargetProperty);
            if (memberType == null)
                throw new InvalidOperationException("Invalid TargetProperty");

            if (typeof(Delegate).IsAssignableFrom(memberType))
            {
                return CreateProxyMethod(memberType, provider.MethodProviderHandler);
            }

            return memberType == typeof(ICommand) ? new CommandProvider(provider) : null;
        }

        private static Delegate CreateProxyMethod(Type handlerType, MethodProviderHandler handler)
        {
            int i = 0;

            IEnumerable<ParameterInfo> parameterInfos = handlerType.GetMethod("Invoke").GetParameters();
            ParameterExpression[] parameters = parameterInfos.Select(p => Expression.Parameter(p.ParameterType, "x" + (i++))).ToArray();

            var arguments = Expression.NewArrayInit(typeof(object), parameters);
            var body = Expression.Call(Expression.Constant(handler), handler.GetType().GetMethod("Invoke"), new Expression[] { arguments });
            var lambda = Expression.Lambda(body, parameters);

            return Delegate.CreateDelegate(handlerType, lambda.Compile(), "Invoke", false);
        }

        private static Type GetMemberType(object targetProperty)
        {
            if (targetProperty is DependencyProperty)
            {
                return (targetProperty as DependencyProperty).PropertyType;
            }
            if (targetProperty is MemberInfo)
            {
                if (targetProperty is EventInfo) return (targetProperty as EventInfo).EventHandlerType;
                if (targetProperty is PropertyInfo) return (targetProperty as PropertyInfo).PropertyType;
            }

            return null;
        }
    }
}