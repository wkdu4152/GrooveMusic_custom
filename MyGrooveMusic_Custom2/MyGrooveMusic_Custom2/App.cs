﻿using MyGrooveMusic.View;
using Xamarin.Forms;

namespace MyGrooveMusic
{
    public class App : Application
    {
        public FileInfoPage Page { get; set; }
        public App()
        {
            // The root page of your application
            Page = new FileInfoPage();
            
            MainPage = new NavigationPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
