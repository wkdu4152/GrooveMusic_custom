﻿
using MyGrooveMusic.source.ViewModel;
using Xamarin.Forms;

namespace MyGrooveMusic
{
    public partial class MainPage : ContentPage
    {
        public WebView WebView { get; private set; }
        public MainPage()
        {
            InitializeComponent();
            WebView = this.webPageView;
            BindingContext = new MainPageViewModel();
        }
        public void RemoveWebView()
        {
            OnChildRemoved(this.webPageView);
        }
    }
}
