
namespace MyGrooveMusic.source.container
{
    public class GrooveMusicAutheResponse
    {
        public string Token_Type;
        public string Access_Token;
        public string Expires_In;
        public string Scope;
    }
}