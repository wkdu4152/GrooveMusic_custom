namespace MyGrooveMusic.source.container
{
    public struct OnedriveTokenContainer
    {
        public string Token_Type { get; set; }
        public string Expires_In { get; set; }
        public string Scope { get; set; }
        public string Access_Token { get; set; }
        public string Refresh_Token { get; set; }
        public string Authentication_Token { get; set; }
        public string IssuedDate { get; set; }
    }
}