﻿using MyGrooveMusic.source.container;
using System.Collections.Generic;

namespace MyGrooveMusic.container
{
    public class PlaylistMeta
    {
        public bool IsLoadComplete { get; set; }
        public List<OneDriveInfoContainer> MetaDatas { get; set; }
    }
}
