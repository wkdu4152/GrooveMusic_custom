﻿namespace MyGrooveMusic.source.container
{
    public class OneDriveFile
    {
        public string Odata;
        public OneDriveInfoContainer Value;
    }
    public class OneDriveInfoContainer
    {
        public string DownloadUrl;
        public string ID;
        public string Name;
        public Parent parentReference;
        public long Size;
        public string WebUrl;
        public FileInfo File;
        public FolderInfo Folder;
        public bool Selected;
    }
    public struct Parent
    {
        public string DriveId;
        public string ID;
        public string Path;
    }

    public struct FileInfo
    {
        public FileHash Hashes;
        public string MimeType;
    }

    public struct FileHash
    {
        public string Crc32Hash;
        public string Sha1Hash;
    }

    public struct FolderInfo
    {
        public uint ChileCount;
    }
}
