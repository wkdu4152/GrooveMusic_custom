﻿using System.Collections.Generic;
using MyGrooveMusic.source.container;
using MyGrooveMusic.container;

namespace MyGrooveMusic.source
{
    public static class DataManager
    {
        public static readonly string ONEDRIVE_TOKEN_URL = "https://login.live.com/oauth20_token.srf";
        public static readonly string ONEDRIVE_AUTHORITY_URL = "https://login.live.com/oauth20_authorize.srf";
        public static readonly string REQUST_ONEDRIVE_SERVICE_URL = "https://api.onedrive.com/v1.0/drive/root:/음악/음악라이브러리 파일:/children";
        public static readonly string REQUST_ONEDRIVE_MEDIA_FILES_URL = "https://api.onedrive.com/v1.0/drive/root/view.search(q='{0}')";
        //public static readonly string REQUST_ONEDRIVE_SERVICE_URL = "https://api.onedrive.com/v1.0/drive/root/children";
        public static readonly string REQUST_SPECIFIC_ID_SEARCH_URL = "https://api.onedrive.com/v1.0/drive/items/{0}/children?access_token={1}";
        public static readonly string REQUEST_SPECIFIC_PATH_SEARCH_URL = "https://api.onedrive.com/v1.0{0}:/children?access_token={1}";

        public static readonly string CLIENT_ID = "8d614557-34aa-400a-bfa2-459891ca6a7c";
        public static readonly string CLIENT_SECRET = "zpyFSNhpiHZUFZj5WpXiSwh";

        public static Dictionary<string, OneDriveInfoContainer> FILES = new Dictionary<string, OneDriveInfoContainer>();
        public static Dictionary<string, List<OneDriveInfoContainer>> PLAYLIST = new Dictionary<string, List<OneDriveInfoContainer>>();
        public static Dictionary<string, PlaylistMeta> PLAYLIST_METADATA = new Dictionary<string, PlaylistMeta>();
        public static List<OneDriveInfoContainer> PLAYED_QUEUE = new List<OneDriveInfoContainer>();

        public static void CreatePlayList(string playListName)
        {
            if (!string.IsNullOrEmpty(playListName))
            {
                PLAYLIST_METADATA.Add(playListName, new PlaylistMeta());
                PLAYLIST_METADATA[playListName].MetaDatas = new List<OneDriveInfoContainer>();
            }
        }

        public static string WritePlayListFile(string directory, string playListName)
        {
            string result = string.Empty;
            //File playListFile = new File(directory + "/OnedrivePlayer/playlist/" + string.Format("{0}.opl", playListName));
            
            //try
            //{
            //    if (!playListFile.Exists())
            //    {
            //        playListFile.CreateNewFile();
            //    }

            //    // TODO write playlist infomation
            //    if (playListFile.CanWrite())
            //    {
            //        JsonSerializerSettings setting = new JsonSerializerSettings();
            //        setting.Converters.Add(new OnedriveInfoJsonConverter());

            //        string serializedData = JsonConvert.SerializeObject(PLAYLIST_METADATA[playListName].MetaDatas, Formatting.Indented, setting);
            //        serializedData = "{\"value\":" + serializedData + "}";
            //        FileOutputStream writer = new FileOutputStream(playListFile);

            //        writer.Write(System.Text.Encoding.UTF8.GetBytes(serializedData));
            //        writer.Close();
            //        result = "DONE\n" + serializedData;
            //    }
            //}
            //catch (Exception e)
            //{
            //    result = e.Message + "\n" + e.StackTrace;
            //}

            return result;
        }

        public static string LoadPlayList(string directory)
        {
            string result = string.Empty;
            //File playListDirectory = new File(directory + "/OnedrivePlayer/playlist/");
           
            //if (!playListDirectory.Exists())
            //{
            //    playListDirectory.Mkdirs();
            //}

            //File[] childFiles = playListDirectory.ListFiles();
            
            //if(childFiles != null)
            //{
            //    foreach (File child in childFiles)
            //    {
            //        try
            //        {
            //            BufferedReader reader = new BufferedReader(new FileReader(child));
            //            string temp = null;
            //            StringBuilder builder = new StringBuilder();
            //            while ((temp = reader.ReadLine()) != null)
            //            {
            //                builder.Append(temp);
            //            }

            //            reader.Close();

            //            string playlistData = builder.ToString();
            //            result = playlistData + "\n";

            //            PlaylistMeta playlistMeta = new PlaylistMeta();
            //            playlistMeta.MetaDatas = Utils.CommonUtil.GetParsedOndriveItems(playlistData);

            //            string fileName = child.Name;
            //            string playListName = fileName.Substring(0, fileName.Length - 4);
            //            PLAYLIST_METADATA.Add(playListName, playlistMeta);
            //        }
            //        catch (Exception e)
            //        {
            //            result += e.Message + "\n" + e.StackTrace;
            //        }
            //    }
            //}
          
            return result;
        }

        public static void DeletePlayListFile(string directory, string playListName)
        {
            //File playListFile = new File(directory + "/OnedrivePlayer/playlist/"+ string.Format("{0}.opl", playListName));

            //playListFile.Delete();
            PLAYLIST.Remove(playListName);
        }
    }
}
