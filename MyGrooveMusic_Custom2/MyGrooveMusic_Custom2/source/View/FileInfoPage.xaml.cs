﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MyGrooveMusic.View
{
    public partial class FileInfoPage : ContentView
    {
        public WebView WebView { get; set; }
        public FileInfoPage()
        {
            InitializeComponent();
            WebView = this.webView;
        }
    }
}
