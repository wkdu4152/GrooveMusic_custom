﻿using System.ComponentModel;
using Xamarin.Forms;
using MyGrooveMusic.source.service;
using MyGrooveMusic.source.container;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace MyGrooveMusic.source.ViewModel
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        #region NotifyProperty method & event
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region properties
        private string mWebViewSource = string.Empty;
        public string WebViewSource
        {
            get
            {
                return mWebViewSource;
            }
            private set
            {
                if (value != null)
                {
                    mWebViewSource = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion

        #region MVVM Event Commands

        #endregion

        #region MVVM Event Methods
        public async void webPageView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Source")
            {
                WebView view = sender as WebView;
                if (view != null)
                {
                    string url = ((UrlWebViewSource)view.Source).Url;
                    if (url.Contains("code="))
                    {
                        view.IsVisible = false;
                        string[] splitedCode = url.Split(new char[] { '=' });

                        string code = splitedCode[1];
                        await OneDriveService.GetAccessToken(code);
                        await OneDriveService.StartRefreshCheck();
                        List<OneDriveInfoContainer> fileInfo = await OneDriveService.GetOneDriveFiles();

                        if (fileInfo != null)
                        {
                            // file information process
                            foreach (OneDriveInfoContainer info in fileInfo)
                            {
                                DataManager.FILES.Add(info.Name, info);
                            }
                        }
                    }
                }
            }
        }

        public async void WebView_PropertyChanged()
        {
            if (mWebViewSource != null)
            {
                string url = ((UrlWebViewSource)mWebViewSource).Url;

                if (url.Contains("code="))
                {
                    string[] splitedCode = url.Split(new char[] { '=' });

                    string code = splitedCode[1];
                    await OneDriveService.GetAccessToken(code);
                    await OneDriveService.StartRefreshCheck();
                    List<OneDriveInfoContainer> fileInfo = await OneDriveService.GetOneDriveFiles();

                    if (fileInfo != null)
                    {
                        // file information process
                        foreach (OneDriveInfoContainer info in fileInfo)
                        {
                            DataManager.FILES.Add(info.Name, info);
                        }
                    }
                }
            }
        }
        #endregion

        public MainPageViewModel()
        {
            CallOneDriveAuthentication();
        }

        private async void CallOneDriveAuthentication()
        {
            string onedriveAuthUrl = await OneDriveService.GetAuthenticationPage();
            WebViewSource = onedriveAuthUrl;
        }
    }
}
