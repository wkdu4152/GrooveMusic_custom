﻿using MyGrooveMusic.source;
using MyGrooveMusic.source.container;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace MyGrooveMusic.JsonConverters
{
    public class OnedriveInfoJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.Equals(typeof(OneDriveInfoContainer));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject json = JObject.Load(reader);
            OneDriveInfoContainer container = new OneDriveInfoContainer();

            container.DownloadUrl = (string)json["@content.downloadUrl"];
            container.ID = (string)json["id"];
            container.Size = (long)json["size"];
            container.Name = (string)json["name"];
            container.WebUrl = (string)json["webUrl"];

            JObject parent = json["parentReference"] as JObject;

            if (parent != null)
            {
                container.parentReference.DriveId = (string)parent["driveId"];
                container.parentReference.ID = (string)parent["id"];
                container.parentReference.Path = (string)parent["path"];
            }

            JObject file = json["file"] as JObject;

            if (file != null)
            {
                JObject fileHash = file["hashes"] as JObject;
                if (fileHash != null)
                {
                    container.File.Hashes.Crc32Hash = (string)fileHash["crc32Hash"];
                    container.File.Hashes.Sha1Hash = (string)fileHash["sha1Hash"];
                }
                container.File.MimeType = (string)file["mimeType"];
            }

            JObject folder = json["folder"] as JObject;
            if (folder != null)
            {
                container.Folder.ChileCount = (uint)folder["childCount"];
            }

            return container;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            OneDriveInfoContainer convertTarget = value as OneDriveInfoContainer;

            if (convertTarget != null)
            {
                writer.WriteStartObject();

                writer.WritePropertyName("@content.downloadUrl");
                writer.WriteValue(convertTarget.DownloadUrl);

                writer.WritePropertyName("id");
                writer.WriteValue(convertTarget.ID);

                writer.WritePropertyName("name");
                writer.WriteValue(convertTarget.Name);

                writer.WritePropertyName("size");
                writer.WriteValue(convertTarget.Size);

                writer.WritePropertyName("webUrl");
                writer.WriteValue(convertTarget.WebUrl);

                #region parent struct serialize
                writer.WritePropertyName("parentReference");
                writer.WriteStartObject();

                writer.WritePropertyName("driveId");
                writer.WriteValue(convertTarget.parentReference.DriveId);

                writer.WritePropertyName("id");
                writer.WriteValue(convertTarget.parentReference.ID);

                writer.WritePropertyName("path");
                writer.WriteValue(convertTarget.parentReference.Path);
                writer.WriteEndObject();
                #endregion

                #region file struct serialize
                writer.WritePropertyName("file");
                writer.WriteStartObject();

                writer.WritePropertyName("hashes");
                writer.WriteStartObject();

                writer.WritePropertyName("crc32Hash");
                writer.WriteValue(convertTarget.File.Hashes.Crc32Hash);

                writer.WritePropertyName("sha1Hash");
                writer.WriteValue(convertTarget.File.Hashes.Sha1Hash);
                writer.WriteEndObject();

                writer.WritePropertyName("mimeType");
                writer.WriteValue(convertTarget.File.MimeType);
                writer.WriteEndObject();
                #endregion

                #region folder struct serialize
                writer.WritePropertyName("folder");
                writer.WriteStartObject();

                writer.WritePropertyName("childCount");
                writer.WriteValue(convertTarget.Folder.ChileCount);
                writer.WriteEndObject();
                #endregion

                writer.WriteEndObject();
            }
        }
    }
}
