﻿using MyGrooveMusic.source.container;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace MyGrooveMusic.JsonConverters
{
    public class OnedriveTokenJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.Equals(typeof(OnedriveTokenContainer));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject json = JObject.Load(reader);

            OnedriveTokenContainer container = new OnedriveTokenContainer();
            
            container.Token_Type = (string)json["token_type"];
            container.Access_Token = (string)json["access_token"];
            container.Refresh_Token = (string)json["refresh_token"];
            container.Scope = (string)json["scope"];
            container.Expires_In = (string)json["expires_in"];
            container.Authentication_Token = (string)json["authentication_token"];
            container.IssuedDate = DateTime.Now.ToString("yyyyMMddHHmmss");

            return container;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
