using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using MyGrooveMusic.source.container;
using MyGrooveMusic.JsonConverters;
using MyGrooveMusic.Utils;
using System.Globalization;
using MyGrooveMusic.container;

namespace MyGrooveMusic.source.service
{
    public static class OneDriveService
    {
        private static OnedriveTokenContainer mOnedriveToken;
        private static bool mRefreshCheck = true;

        public static async Task StartRefreshCheck()
        {
            await Task.Factory.StartNew(async () =>
            {
                while (mRefreshCheck)
                {
                    DateTime issuedDate = DateTime.MaxValue;

                    if (DateTime.TryParseExact(mOnedriveToken.IssuedDate, "yyyyMMddHHmmss", CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out issuedDate))
                    {
                        DateTime nowDate = DateTime.Now;

                        TimeSpan leftTime = nowDate - issuedDate;
                        if (leftTime.TotalSeconds > int.Parse(mOnedriveToken.Expires_In))
                        {
                            await RefreshAccessToken();
                        }
                    }
                    await Task.Delay(1000);
                }
            });
        }

        public static void StopRefreshCheck()
        {
            mRefreshCheck = false;
        }

        /// <summary>
        /// requst Authentication page
        /// </summary>
        /// <returns></returns>
        public static async Task<string> GetAuthenticationPage()
        {
            string url = string.Format("{0}?client_id={1}&scope=wl.signin%20wl.offline_access%20wl.skydrive&response_type=code&redirect_uri=urn:ietf:wg:oauth:2.0:oob", DataManager.ONEDRIVE_AUTHORITY_URL, DataManager.CLIENT_ID);

            HttpWebRequest authenRequest = (HttpWebRequest)WebRequest.Create(url);
            string authUrl = string.Empty;

            using (WebResponse response = await WebUtil.GetResponseAsync(authenRequest))
            {
                if(response != null) {
                    Uri responseUri = response.ResponseUri;
                    authUrl = responseUri.AbsoluteUri;
                }
            }

            return authUrl;
        }

        /// <summary>
        ///     requst onedrive access token
        /// </summary>
        /// <param name="code">
        ///     onedrive authentication code
        /// </param>
        /// <returns></returns>
        public static async Task GetAccessToken(string code)
        {
            string responseResult = string.Empty;

            UTF8Encoding encoding = new UTF8Encoding();

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(DataManager.ONEDRIVE_TOKEN_URL);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";

            string data = string.Format("client_id={0}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&code={1}&grant_type=authorization_code", DataManager.CLIENT_ID, code);

            byte[] buffer = new byte[encoding.GetBytes(data).Length];
            buffer = encoding.GetBytes(data);

            try
            {
                using (Stream stream = await WebUtil.GetRequestStreamAsync(webRequest))
                {
                    if(stream != null)
                    {
                        stream.Write(buffer, 0, buffer.Length);
                    }
                }

                responseResult = await GetInfoResponse(webRequest);

                if (!string.IsNullOrEmpty(responseResult))
                {
                    JsonSerializerSettings setting = new JsonSerializerSettings();
                    setting.Converters.Add(new OnedriveTokenJsonConverter());
                    mOnedriveToken = JsonConvert.DeserializeObject<OnedriveTokenContainer>(responseResult, setting);
                    //Log.Debug("Issudate", mOnedriveToken.IssuedDate);
                    //Log.Debug("token_life", mOnedriveToken.Expires_In);
                }
            }
            catch (WebException e)
            {
                string error = string.Empty;
                if (e.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)e.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
                        }
                    }
                }
            }
        }

        public static async Task<List<OneDriveInfoContainer>> DownloadPlayListItems(PlaylistMeta metaData)
        {
            List<OneDriveInfoContainer> playlistFiles = new List<OneDriveInfoContainer>();
            if (!metaData.IsLoadComplete)
            {
                List<OneDriveInfoContainer> folderChildren = metaData.MetaDatas.FindAll((item) =>
                {
                    if (item.Folder.ChileCount > 0)
                    {
                        return true;
                    }
                    return false;
                });
                List<OneDriveInfoContainer> items = metaData.MetaDatas.FindAll((item) =>
                {
                    if (!string.IsNullOrEmpty(item.File.Hashes.Crc32Hash))
                    {
                        return true;
                    }
                    return false;
                });

                // refresh item information
                for (int itemIndex = 0; itemIndex < items.Count; ++itemIndex)
                {
                    List<OneDriveInfoContainer> result = await GetOneDriveFilesForPaths(items[itemIndex].parentReference.Path + "/" + items[itemIndex].Name);
                    playlistFiles.AddRange(result);
                }

                for (int itemIndex = 0; itemIndex < folderChildren.Count; ++itemIndex)
                {
                    if (folderChildren[itemIndex].Folder.ChileCount > 0)
                    {
                        List<OneDriveInfoContainer> result = await getAllChildrenFiles(folderChildren[itemIndex].ID);
                        List<OneDriveInfoContainer> playableFiles = result.FindAll((info) =>
                        {
                            return CommonUtil.IsCanPlayFile(info.Name);
                        });
                        playlistFiles.AddRange(playableFiles);
                    }
                }

                metaData.IsLoadComplete = true;
            }
            return playlistFiles;
        }

        private static async Task<List<OneDriveInfoContainer>> getAllChildrenFiles(string id)
        {
            List<OneDriveInfoContainer> childrenFiles = new List<OneDriveInfoContainer>();
            List<OneDriveInfoContainer> rootChild = await GetOneDriveFilesForId(id);

            for (int childIndex = 0; childIndex < rootChild.Count; ++childIndex)
            {
                if (rootChild[childIndex].Folder.ChileCount > 0)
                {
                    List<OneDriveInfoContainer> temp = await getAllChildrenFiles(rootChild[childIndex].ID);
                    childrenFiles.AddRange(temp);
                }
                else
                {
                    childrenFiles.Add(rootChild[childIndex]);
                }
            }

            return childrenFiles;
        }

        private static async Task<string> GetInfoResponse(HttpWebRequest webRequest)
        {
            string readedData = string.Empty;
            WebResponse response = await WebUtil.GetResponseAsync(webRequest);

            if (response != null)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                if (reader != null)
                {
                    readedData = reader.ReadToEnd();

                    reader.Dispose();
                }

                response.Dispose();
            }

            return readedData;
        }

        /// <summary>
        /// Get onedrive data to root folder
        /// reference by https://dev.onedrive.com/resources/item.htm
        /// </summary>
        /// <returns>
        /// OneDrive file list
        /// </returns>
        public static async Task<List<OneDriveInfoContainer>> GetOneDriveFiles()
        {
            try
            {
                string url = string.Format("{0}?access_token={1}", DataManager.REQUST_ONEDRIVE_SERVICE_URL, mOnedriveToken.Access_Token);
                //Log.Debug("url", url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Method = "GET";

                string readedData = await GetInfoResponse(webRequest);

                if (!string.IsNullOrEmpty(readedData))
                {
                    return CommonUtil.GetParsedOndriveItems(readedData);
                }
            }
            catch (WebException e)
            {
                string error = string.Empty;
                if (e.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)e.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
                        }
                    }
                }
            }

            return null;
        }

        public static async Task<List<OneDriveInfoContainer>> GetOneDriveFilesForPaths(string path)
        {
            string url = string.Format(DataManager.REQUEST_SPECIFIC_PATH_SEARCH_URL, path, mOnedriveToken.Access_Token);
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.Method = "GET";
                string readedData = await GetInfoResponse(webRequest);

                if (!string.IsNullOrEmpty(readedData))
                {
                    return CommonUtil.GetParsedOndriveItems(readedData);
                }
            }
            catch (WebException e)
            {
                string error = string.Empty;
                if (e.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)e.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get Onedrvie data to spcific folder
        /// reference by https://dev.onedrive.com/resources/item.htm
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<List<OneDriveInfoContainer>> GetOneDriveFilesForId(string itemId)
        {
            try
            {
                string url = string.Format(DataManager.REQUST_SPECIFIC_ID_SEARCH_URL, itemId, mOnedriveToken.Access_Token);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Method = "GET";

                string readedData = await GetInfoResponse(webRequest);

                if (readedData != null)
                {
                    return CommonUtil.GetParsedOndriveItems(readedData);
                }
            }
            catch (WebException e)
            {
                string error = string.Empty;
                if (e.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)e.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
                        }
                    }
                }
            }

            return null;
        }

        public static async Task<byte[]> DownloadFile(string downloadUrl, long fileSize)
        {
            byte[] buffer = new byte[fileSize];
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(downloadUrl);

            using (WebResponse response = await WebUtil.GetResponseAsync(webRequest))
            {
                using (Stream stream = response.GetResponseStream())
                {
                    stream.Read(buffer, 0, buffer.Length);
                }
            }

            return buffer;
        }

        /// <summary>
        /// refresh access token
        /// reference by : https://dev.onedrive.com/auth/msa_oauth.htm
        /// </summary>
        /// <returns></returns>
        public static async Task RefreshAccessToken()
        {
            UTF8Encoding encoding = new UTF8Encoding();
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(DataManager.ONEDRIVE_TOKEN_URL);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";

            string data = string.Format("client_id={0}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&refresh_token={1}&grant_type=refresh_token", DataManager.CLIENT_ID, mOnedriveToken.Refresh_Token);

            byte[] buffer = new byte[encoding.GetBytes(data).Length];
            buffer = encoding.GetBytes(data);

            string responseResult = string.Empty;

            try
            {
                using (Stream stream = await WebUtil.GetRequestStreamAsync(webRequest))
                {
                    stream.Write(buffer, 0, buffer.Length);
                }

                responseResult = await GetInfoResponse(webRequest);
                if (!string.IsNullOrEmpty(responseResult))
                {
                    JsonSerializerSettings setting = new JsonSerializerSettings();
                    setting.Converters.Add(new OnedriveTokenJsonConverter());
                    mOnedriveToken = JsonConvert.DeserializeObject<OnedriveTokenContainer>(responseResult, setting);
                }
            }
            catch (WebException e)
            {
                string error = string.Empty;
                if (e.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)e.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
                        }
                    }
                }
            }
        }
    }
}