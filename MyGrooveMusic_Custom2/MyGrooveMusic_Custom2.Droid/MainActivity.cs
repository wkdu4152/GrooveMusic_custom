﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Android.Content;
using Android.Util;
using Newtonsoft.Json;
using MyGrooveMusic.Droid.source;
using Xamarin.Forms;
using System.Collections.Generic;
using MyGrooveMusic.source.container;
using MyGrooveMusic.source;
using MyGrooveMusic.source.service;
using MyGrooveMusic_Custom2.Droid;
using MyGrooveMusic.View;

[assembly: UsesPermission(Android.Manifest.Permission.Internet)]

namespace MyGrooveMusic.Droid
{
    [Activity(Label = "MyGrooveMusic", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        // private IPicker picker;
        source.GrooveMusicAuthenticator auth = null;
        private FileInfoPage mMainPage;

        private GrooveMusicAutheResponse mGrooveAPiKey = null;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Xamarin.Forms.Forms.Init(this, bundle);

            App app = new App();
            mMainPage = app.Page;
            // mMainPage.WebView.PropertyChanged += WebView_PropertyChanged;

            CallOneDriveAuthentication();
            //LoadApplication(app);
        }

        private async void WebView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Source")
            {
                WebView view = sender as WebView;
                if (view != null)
                {
                    string url = ((UrlWebViewSource)view.Source).Url;
                    Log.Info("Redirecting!", ((UrlWebViewSource)view.Source).Url);
                    if (url.Contains("code="))
                    {
                        view.IsVisible = false;
                        string[] splitedCode = url.Split(new char[] { '=' });

                        string code = splitedCode[1];
                        await OneDriveService.GetAccessToken(code);
                        await OneDriveService.StartRefreshCheck();
                        List<OneDriveInfoContainer> fileInfo = await OneDriveService.GetOneDriveFiles();

                        if (fileInfo != null)
                        {
                            // file information process
                            foreach (OneDriveInfoContainer info in fileInfo)
                            {
                                DataManager.FILES.Add(info.Name, info);
                            }
                        }

                        // mMainPage.RemoveWebView();
                        
                        StartActivity(new Intent(this, typeof(PlayerActivity)));
                    }
                }
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
        }

        private async void CallOneDriveAuthentication()
        {
            string onedriveAuthUrl = await OneDriveService.GetAuthenticationPage();
            mMainPage.WebView.Source = onedriveAuthUrl;
        }

        private async void CallGrooveAuthentication()
        {
            if (auth != null)
            {
                string jsonData = await auth.GetAuthenticationCode();
                if (!string.IsNullOrEmpty(jsonData))
                {
                    mGrooveAPiKey = JsonConvert.DeserializeObject<GrooveMusicAutheResponse>(jsonData);
                    Toast.MakeText(this, string.Format("successfuly groove music api key. key: {0}", jsonData), ToastLength.Long);
                }
            }
        }
    }
}

