
using Android.App;
using Android.OS;
using Android.Widget;
using MyGrooveMusic.Droid.source;
using Android.Views;
using System;
using MyGrooveMusic.Droid.source.Fragments;
using Android.Util;
using MyGrooveMusic.source;
using System.Collections.Generic;
using System.Collections;
using MyGrooveMusic.source.container;
using MyGrooveMusic.Droid.source.ItemContainer;
using MyGrooveMusic_Custom2.Droid;

namespace MyGrooveMusic.Droid
{
    [Activity(Label = "FileInfoActivity")]
    public class PlayerActivity : Activity, Android.Views.View.IOnClickListener
    {
        private ICustomFragment mNowFragment = null;
        private FolderFragment mFolderFragment = null;
        private PlayListFragment mPlaylistFragment = null;
        private PlayerFragment mPlayerFragment = null;
        private Android.Views.View playlistDialog = null;
        private TabHost mHost = null;
        private AlertDialog mDialogBuilder = null;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MainActivityView);
            SetTitle(Resource.String.ActionBarTitle);
            ActionBar.Hide();

            // create fragments
            mFolderFragment = new FolderFragment(this);
            mPlaylistFragment = new PlayListFragment(this);
            mPlayerFragment = new PlayerFragment(this);

            mHost = FindViewById<TabHost>(Resource.Id.tabHost);
            mHost.TabChanged += MHost_TabChanged;
            mHost.Setup();

            TabHost.TabSpec spec = mHost.NewTabSpec(GetString(Resource.String.playerTab));
            spec.SetContent(Resource.Id.playerContainer);
            spec.SetIndicator(GetString(Resource.String.playerTab));
            mHost.AddTab(spec);

            spec = mHost.NewTabSpec(GetString(Resource.String.folderTab));
            spec.SetContent(Resource.Id.fragmentContainer);
            spec.SetIndicator(GetString(Resource.String.folderTab));

            mHost.AddTab(spec);

            spec = mHost.NewTabSpec(GetString(Resource.String.playTab));
            spec.SetContent(Resource.Id.playListContainer);
            spec.SetIndicator(GetString(Resource.String.playTab));
            mHost.AddTab(spec);

            // start MediaPlayer service
            MediaService.GetInstance().OnBind(this.Intent);
            DataManager.LoadPlayList(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath);

            FindViewById<Button>(Resource.Id.removeBtn).Click += LongTouchButton_Click;
            FindViewById<Button>(Resource.Id.playlistAddBtn).Click += LongTouchButton_Click;
        }

        private void MHost_TabChanged(object sender, TabHost.TabChangeEventArgs e)
        {
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            
            if (e.TabId.Equals(GetString(Resource.String.folderTab)))
            {
                mNowFragment = mFolderFragment;
                transaction.Replace(Resource.Id.fragmentContainer, mFolderFragment);
                transaction.Commit();
            }
            else if (e.TabId.Equals(GetString(Resource.String.playTab)))
            {
                mNowFragment = mPlaylistFragment;
                transaction.Replace(Resource.Id.playListContainer, mPlaylistFragment);
                transaction.Commit();
            }
            else if (e.TabId.Equals(GetString(Resource.String.playerTab)))
            {
                mNowFragment = mPlayerFragment;
                transaction.Replace(Resource.Id.playerContainer, mPlayerFragment);
                transaction.Commit();
            }

            ResetLongclickButton();
            mNowFragment.ResetItemSelectionMode();
        }

        private void ResetLongclickButton()
        {
            LinearLayout extensionLayout = FindViewById<LinearLayout>(Resource.Id.longClickBtnContainer);
            if (extensionLayout != null)
            {
                extensionLayout.Visibility = ViewStates.Gone;
                FindViewById<LinearLayout>(Resource.Id.playlistLongTouchBtns).Visibility = ViewStates.Gone;
                FindViewById<LinearLayout>(Resource.Id.folderLongClickBtns).Visibility = ViewStates.Gone;
            }
        }

        private void LongTouchButton_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button.Id == Resource.Id.playlistAddBtn)
            {
                // store paly list
                if (mNowFragment.GetType().Equals(typeof(FolderFragment)))
                {
                    playlistDialog = LayoutInflater.Inflate(Resource.Layout.AddPlayListDialog, null);
                    ListView playListView = playlistDialog.FindViewById<ListView>(Resource.Id.addPlayLists);
                    RedneringAddPlayListDialog(playListView);

                    Button addListBtn = playlistDialog.FindViewById<Button>(Resource.Id.addListBtn);

                    // playlist add button click event
                    addListBtn.Click += AddListBtn_Click;

                    mDialogBuilder = new AlertDialog.Builder(this).Create();
                    mDialogBuilder.SetView(playlistDialog);
                    mDialogBuilder.Show();
                }
            }
            else if (button.Id == Resource.Id.removeBtn)
            {
                // TODO delete playlist metadata
                PlaylistItem[] items = (PlaylistItem[])mNowFragment.GetCheckedList();
                foreach(PlaylistItem item in items)
                {
                    DataManager.DeletePlayListFile(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, item.PlaylistName);
                    DataManager.PLAYLIST_METADATA.Remove(item.PlaylistName);
                }

                mNowFragment.Refresh();
            }
        }

        private void AddListBtn_Click(object sender, EventArgs e)
        {
            // show create playlist dialog
            AlertDialog builder = new AlertDialog.Builder(this).Create();
            Android.Views.View createListView = LayoutInflater.Inflate(Resource.Layout.AddListDialogView, null);
            // add button event
            createListView.FindViewById<Button>(Resource.Id.createListBtn).Click += (cSender, ce) =>
            {
                string playListName = createListView.FindViewById<TextView>(Resource.Id.playListName).Text;

                if (!DataManager.PLAYLIST.ContainsKey(playListName))
                {
                    DataManager.CreatePlayList(playListName);

                    ListView playListView = playlistDialog.FindViewById<ListView>(Resource.Id.addPlayLists);
                    RedneringAddPlayListDialog(playListView);

                    builder.Cancel();
                }
                else
                {
                    Toast.MakeText(createListView.Context, "this play list already exist", ToastLength.Short);
                }
            };
            // cancellation button event
            createListView.FindViewById<Button>(Resource.Id.cancelCListBtn).Click += (canSender, cane) =>
            {
                builder.Cancel();
            };
            builder.SetView(createListView);
            builder.Show();
        }

        private void RedneringAddPlayListDialog(ListView view)
        {
            view.ChoiceMode = ChoiceMode.Single;
            IEnumerator itor = DataManager.PLAYLIST_METADATA.Keys.GetEnumerator();
            List<string> playLists = new List<string>();

            while (itor.MoveNext())
            {
                playLists.Add(itor.Current as string ?? (string)itor.Current);
            }

            if (playLists.Count == 0)
            {
                view.Visibility = ViewStates.Gone;
            }
            else
            {
                view.Visibility = ViewStates.Visible;
            }
            view.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, playLists);
            view.ItemClick += View_ItemClick;
        }

        private void View_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ListView listView = sender as ListView;
            FolderFragment fragment = mNowFragment as FolderFragment;
            string item = (string)listView.Adapter.GetItem(e.Position);

            DataManager.PLAYLIST_METADATA[item].MetaDatas.AddRange((OneDriveInfoContainer[])fragment.GetCheckedList());
            DataManager.PLAYLIST_METADATA[item].IsLoadComplete = false;

            fragment.InitializeSelectedItem();
            mDialogBuilder.Dismiss();

            string createResult = DataManager.WritePlayListFile(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, item);
            Log.Debug("playlistWrite", createResult);
        }

        /// <summary>
        /// back button event
        /// </summary>
        public override void OnBackPressed()
        {
            ResetLongclickButton();
            mNowFragment.MoveBackParent();
        }

        public void OnClick(Android.Views.View v)
        {
        }
    }
}