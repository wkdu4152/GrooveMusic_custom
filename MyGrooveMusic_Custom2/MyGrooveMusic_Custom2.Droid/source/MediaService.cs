using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Media;
using MyGrooveMusic.source;
using System.Collections.Generic;
using System.Timers;

namespace MyGrooveMusic.Droid.source
{
    public class MediaServiceBinder : Binder
    {

    }

    public class PlayFileInfo : EventArgs
    {
        public string FileName;
        public int Duration;
    }

    internal enum eAction
    {
        Now, Prev, Next
    }

    public class MediaService : Service, MediaPlayer.IOnErrorListener
    {
        public const string PLAY_ACTION = "PLAY";
        public const string NEXT_ACTION = "NEXT";
        public const string PREV_ACTION = "PREV";
        public const string PAUSE_ACTION = "PAUSE";
        public const string RESUME_ACTION = "RESUME";

        private const int PREV_PLAY = 0;
        private const int NOW_PLAY = 1;
        private const int NEXT_PLAY = 2;

        private static object locker = new object();
        private static MediaService instance = null;
        private static MediaPlayer player = new MediaPlayer();
        private static MediaMetadataRetriever mediaMeta = new MediaMetadataRetriever();

        private static bool isPrepared = false;
        private static bool isCompeletePlaying = false;
        private static int playIndex = 0;

        private Timer actionExecutTimer = new Timer(1000);
        private long? mImageCrc = 0;

        public static MediaService GetInstance()
        {
            lock (locker)
            {
                if (instance == null)
                {
                    instance = new MediaService();
                }
            }

            return instance;
        }

        private MediaService()
        {
            player.Completion += MediaPlayer_Completion;
            player.Prepared += MPlayer_Prepared;

            actionExecutTimer.AutoReset = true;
            actionExecutTimer.Enabled = true;
            actionExecutTimer.Elapsed += MActionExecutTimer_Elapsed;
        }

        private static void preparePlay(eAction action)
        {
            switch (action)
            {
                case eAction.Next:
                    if (DataManager.PLAYED_QUEUE != null && DataManager.PLAYED_QUEUE.Count > playIndex)
                    {
                        playIndex++;
                    }
                    break;
                case eAction.Prev:
                    if (DataManager.PLAYED_QUEUE != null && playIndex >= 0)
                    {
                        playIndex--;
                    }
                    break;
                case eAction.Now:
                    playIndex = 0;
                    break;
            }

            if (player.IsPlaying)
            {
                player.Stop();
                player.Reset();
            }
            else
            {
                player.Stop();
            }

            if (DataManager.PLAYED_QUEUE.Count > 0)
            {
                string url = DataManager.PLAYED_QUEUE[playIndex].DownloadUrl;
                if (!string.IsNullOrEmpty(url))
                {
                    try
                    {
                        mediaMeta.SetDataSource(url, new Dictionary<string, string>());

                        player.SetDataSource(url);
                        player.PrepareAsync();
                    }
                    catch (Exception e)
                    {
                        Android.Util.Log.Error("MediaService Error", e.Message);
                        Android.Util.Log.Error("MediaService Error", e.StackTrace);

                        player.Stop();
                        player.Reset();
                    }
                }
            }
        }

        #region overrided methos
        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            string action = intent.Action;
            switch (action)
            {
                case PLAY_ACTION:
                    preparePlay(eAction.Now);
                    break;
                case NEXT_ACTION:
                    preparePlay(eAction.Next);
                    break;
                case PREV_ACTION:
                    preparePlay(eAction.Prev);
                    break;
                case PAUSE_ACTION:
                    if (player.IsPlaying)
                    {
                        player.Pause();
                    }
                    break;
                case RESUME_ACTION:
                    if (!player.IsPlaying)
                    {
                        isCompeletePlaying = true;
                        player.SeekTo(player.CurrentPosition);
                        player.Start();
                    }
                    break;
            }
            if (DataManager.PLAYED_QUEUE != null && DataManager.PLAYED_QUEUE.Count > 0)
            {
                onPlayed(new PlayFileInfo { FileName = DataManager.PLAYED_QUEUE[playIndex].Name });
            };

            return StartCommandResult.Sticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            return new MediaServiceBinder();
        }

        public bool OnError(MediaPlayer mp, [GeneratedEnum] MediaError what, int extra)
        {
            throw new NotImplementedException();
        }

        public override void OnDestroy()
        {
            if (player != null)
            {
                player.Release();
            }
        }
        #endregion

        #region timer loop
        private void MActionExecutTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (isPrepared)
            {
                isPrepared = false;
                player.Start();
                if (DataManager.PLAYED_QUEUE != null && DataManager.PLAYED_QUEUE.Count > 0)
                {
                    onPlayed(new PlayFileInfo { FileName = DataManager.PLAYED_QUEUE[playIndex].Name, Duration = player.Duration });
                };
            }
            else
            {
                #region play next item when end
                if (isCompeletePlaying)
                {
                    int current = player.CurrentPosition / 1000;
                    int duration = player.Duration / 1000;
                    if (current == duration && current >= 0 && duration > 0)
                    {
                        isCompeletePlaying = false;
                        preparePlay(eAction.Next);
                    }
                }
                #endregion
            }

            #region nitoficate current position
            if (player != null)
            {
                if (player.IsPlaying)
                {
                    onChangedCurrentPosition(player.CurrentPosition);
                }
            }
            #endregion

            #region onEmbeddedimage Change check
            if (!string.IsNullOrEmpty(mediaMeta.ExtractMetadata(MetadataKey.Album)))
            {
                long? imageCrc = Utils.CommonUtil.GetCRCValue(mediaMeta.GetEmbeddedPicture());
                if (!imageCrc.HasValue)
                {
                    onReadEmbeddedPicture(null);
                }
                else if (imageCrc != mImageCrc)
                {
                    onReadEmbeddedPicture(mediaMeta.GetEmbeddedPicture());
                    mImageCrc = imageCrc;
                }
            }
            #endregion
        }
        #endregion

        #region event handler
        private static void MPlayer_Prepared(object sender, EventArgs e)
        {
            isPrepared = true;
        }

        private static void MediaPlayer_Completion(object sender, EventArgs e)
        {
            isPrepared = false;
            isCompeletePlaying = true;
        }
        #endregion

        #region events
        public event EventHandler<PlayFileInfo> PlayInfo;
        private void onPlayed(PlayFileInfo e)
        {
            PlayInfo?.Invoke(this, e);
        }

        public event EventHandler<int> CurrentPostionChanged;
        private void onChangedCurrentPosition(int currentPosition)
        {
            CurrentPostionChanged?.Invoke(this, currentPosition);
        }

        public event EventHandler<byte[]> ReadedEmbeddedPicture;
        private void onReadEmbeddedPicture(byte[] picture)
        {
            ReadedEmbeddedPicture.Invoke(this, picture);
        }
        #endregion
    }
}