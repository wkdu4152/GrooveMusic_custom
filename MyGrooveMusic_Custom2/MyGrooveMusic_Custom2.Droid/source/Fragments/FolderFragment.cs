using System.Collections.Generic;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using MyGrooveMusic.source;
using Android.Util;
using MyGrooveMusic.Droid.source.ListAdapters;
using MyGrooveMusic.Utils;
using MyGrooveMusic.source.container;
using MyGrooveMusic.source.service;
using MyGrooveMusic_Custom2.Droid;

namespace MyGrooveMusic.Droid.source.Fragments
{
    public class FolderFragment : Fragment, ICustomFragment
    {
        private ListView mListView = null;
        private Activity mParentActivity = null;
        private List<OneDriveInfoContainer> mItems = new List<OneDriveInfoContainer>();
        private Stack<string> mDepths = new Stack<string>();
        private Android.Views.View mFragmentView = null;

        public FolderFragment(Activity activity)
        {
            mParentActivity = activity;
        }

        public override Android.Views.View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            mFragmentView = inflater.Inflate(Resource.Layout.FragmentView, container, false);
            mListView = (ListView)mFragmentView.FindViewById(Resource.Id.folderList);
            mListView.ItemClick += MListView_ItemClick;
            mListView.ItemLongClick += MListView_ItemLongClick;

            renderingItemView(mFragmentView, ChoiceMode.Single);
            return mFragmentView;
        }

        private void MListView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            // TODO longclick View button visible
            LinearLayout extensionLayout = mParentActivity.FindViewById<LinearLayout>(Resource.Id.longClickBtnContainer);
            if (extensionLayout != null)
            {
                extensionLayout.Visibility = ViewStates.Visible;
                LinearLayout folderLongTouchBtns = mParentActivity.FindViewById<LinearLayout>(Resource.Id.folderLongClickBtns);
                folderLongTouchBtns.Visibility = ViewStates.Visible;
            }
            else
            {
                Log.Debug("extensionLayout", "layout is null");
            }
            renderingItemView(mFragmentView, ChoiceMode.Multiple);
        }

        /// <summary>
        /// item click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (mListView.ChoiceMode == ChoiceMode.Single)
            {
                if (mItems[e.Position].Folder.ChileCount > 0)
                {
                    OneDriveInfoContainer item = mItems[e.Position];
                    List<OneDriveInfoContainer> childItems = await OneDriveService.GetOneDriveFilesForId(item.ID);

                    // reset item list 
                    DataManager.FILES.Clear();

                    // file information process
                    foreach (OneDriveInfoContainer element in childItems)
                    {
                        DataManager.FILES.Add(element.Name, element);
                    }

                    mDepths.Push(item.parentReference.ID);
                    renderingItemView(mFragmentView, ChoiceMode.Single);
                }
                else
                {
                    DataManager.PLAYED_QUEUE = new List<OneDriveInfoContainer>(mItems.FindAll((value) =>
                    {
                        return CommonUtil.IsCanPlayFile(value.Name);
                    }).ToArray());
                    mParentActivity.Intent.SetAction(MediaService.PLAY_ACTION);
                    // HACK: startId parameter using temporary value
                    MediaService.GetInstance().OnStartCommand(mParentActivity.Intent, StartCommandFlags.Retry, 10);
                    Toast.MakeText(View.Context, "play file added", ToastLength.Short).Show();
                }
            }
            else if (mListView.ChoiceMode == ChoiceMode.Multiple)
            {
                CheckBox checkBox = e.View.FindViewById<CheckBox>(Resource.Id.itemCheckBox);
                if (checkBox.Checked)
                {
                    checkBox.Checked = false;
                    mItems[e.Position].Selected = false;
                }
                else
                {
                    checkBox.Checked = true;
                    mItems[e.Position].Selected = true;
                }

                CustomAdapter adapter = mListView.Adapter as CustomAdapter;
                adapter.SetCheckBox(e.Position);
            }
        }

        /// <summary>
        /// item view renderer
        /// </summary>
        private void renderingItemView(Android.Views.View view, ChoiceMode mode)
        {
            mItems.Clear();
            IEnumerator<KeyValuePair<string, OneDriveInfoContainer>> itor = DataManager.FILES.GetEnumerator();

            while (itor.MoveNext())
            {
                KeyValuePair<string, OneDriveInfoContainer> keyValue = itor.Current;
                if (DataManager.FILES[keyValue.Key].File.Hashes.Crc32Hash == null)
                {
                    mItems.Add(DataManager.FILES[keyValue.Key]);
                }
                else if (CommonUtil.IsCanPlayFile(keyValue.Key))
                {
                    mItems.Add(DataManager.FILES[keyValue.Key]);
                }
                else
                {
                    Log.Debug("Debug", keyValue.Key);
                }
            }

            mListView.ChoiceMode = mode;
            mListView.Adapter = new CustomAdapter(view.Context, mItems);
        }

        public async void MoveBackParent()
        {
            if (mDepths.Count > 0)
            {
                List<OneDriveInfoContainer> parentItems = await OneDriveService.GetOneDriveFilesForId(mDepths.Pop());
                DataManager.FILES.Clear();

                foreach (OneDriveInfoContainer element in parentItems)
                {
                    DataManager.FILES.Add(element.Name, element);
                }
            }

            ResetItemSelectionMode();
        }

        public void ResetItemSelectionMode()
        {
            if (mListView != null)
            {
                renderingItemView(mFragmentView, ChoiceMode.Single);
            }
        }

        public object GetCheckedList()
        {
            List<OneDriveInfoContainer> selectedItems = mItems.FindAll((info) =>
            {
                return info.Selected;
            });
            return selectedItems.ToArray();
        }

        public void InitializeSelectedItem()
        {
            List<OneDriveInfoContainer> selectedItems = mItems.FindAll((info) =>
            {
                return info.Selected;
            });

            foreach (OneDriveInfoContainer item in selectedItems)
            {
                item.Selected = false;
            }
            renderingItemView(mFragmentView, mListView.ChoiceMode);
        }

        public void Refresh()
        {
            // renderingItemView(mFragmentView, m)
        }
    }
}