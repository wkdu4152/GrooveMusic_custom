using System;

using Android.App;
using Android.Views;
using Android.OS;
using Android.Widget;
using Android.Graphics;
using MyGrooveMusic_Custom2.Droid;

namespace MyGrooveMusic.Droid.source.Fragments
{
    public class PlayerFragment : Fragment, ICustomFragment
    {
        private Activity mParentActivity = null;
        private Android.Views.View mFragmentView = null;
        private MediaService mServiceProvider = null;
        private SeekBar mPlaySeekBar = null;
        private ImageView mAlbumArtView = null;
        private bool mIsMediaServiceEvent = false;
        private Bitmap mDefaultArt = null;
        private Bitmap mNowAlbumArt = null;

        private bool mPlayBtnToggle = false;
        private int mAlbumArtWidht;
        private int mAlbumArtHeight;

        public PlayerFragment(Activity parentActivity)
        {
            mParentActivity = parentActivity;
            mServiceProvider = MediaService.GetInstance();
            mServiceProvider.PlayInfo += MServiceProvider_PlayInfo;
            mServiceProvider.CurrentPostionChanged += MServiceProvider_CurrentPostionChanged;
            mServiceProvider.ReadedEmbeddedPicture += MServiceProvider_ReadedEmbeddedPicture;
        }

        public override Android.Views.View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            mFragmentView = inflater.Inflate(Resource.Layout.PlayerFragmentView, container, false);
            mFragmentView.FindViewById<Button>(Resource.Id.moveNextItem).Click += PlayerFragmentBtn_Click;
            mFragmentView.FindViewById<Button>(Resource.Id.moveNextList).Click += PlayerFragmentBtn_Click;
            mFragmentView.FindViewById<Button>(Resource.Id.movePastItem).Click += PlayerFragmentBtn_Click;
            mFragmentView.FindViewById<Button>(Resource.Id.movePastList).Click += PlayerFragmentBtn_Click;
            mFragmentView.FindViewById<Button>(Resource.Id.playBtn).Click += PlayerFragmentBtn_Click;
            mAlbumArtView = mFragmentView.FindViewById<ImageView>(Resource.Id.albumArt);
            mDefaultArt = BitmapFactory.DecodeResource(Resources, Resource.Drawable.next_button_blue);

            mFragmentView.ViewTreeObserver.GlobalLayout += ViewTreeObserver_GlobalLayout;

            mPlaySeekBar = mFragmentView.FindViewById<SeekBar>(Resource.Id.playProgress);
            mPlaySeekBar.ProgressChanged += PlayerFragment_ProgressChanged;

            return mFragmentView;
        }

        private void ViewTreeObserver_GlobalLayout(object sender, EventArgs e)
        {
            LinearLayout parent = mFragmentView.FindViewById<LinearLayout>(Resource.Id.playerViewParent);

            mAlbumArtWidht = parent.Width;
            mAlbumArtHeight = (int)(parent.Height * 0.8);

            if (mNowAlbumArt != null)
            {
                mAlbumArtView.SetImageBitmap(Bitmap.CreateScaledBitmap(mNowAlbumArt, mAlbumArtWidht, mAlbumArtHeight, false));
            }
            else
            {
                mAlbumArtView.SetImageBitmap(Bitmap.CreateScaledBitmap(mDefaultArt, mAlbumArtWidht, mAlbumArtHeight, false));
            }
        }

        #region event handlers
        private void MServiceProvider_CurrentPostionChanged(object sender, int e)
        {
            mIsMediaServiceEvent = true;
            mPlaySeekBar.Progress = e;
        }

        private void PlayerFragment_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            if (!mIsMediaServiceEvent)
            {
                // TODO change play position
                mIsMediaServiceEvent = false;
            }
            else
            {
                mIsMediaServiceEvent = false;
            }
        }

        private void MServiceProvider_PlayInfo(object sender, PlayFileInfo e)
        {
            TextView textField = mFragmentView.FindViewById<TextView>(Resource.Id.playFileNameView);
            mPlaySeekBar.Max = e.Duration;
            textField.Text = e.FileName;

#pragma warning disable CS0618 // Type or member is obsolete
            mFragmentView.FindViewById<Button>(Resource.Id.playBtn).Background = mFragmentView.Resources.GetDrawable(Resource.Drawable.pause);
#pragma warning restore CS0618 // Type or member is obsolete
        }

        private void MServiceProvider_ReadedEmbeddedPicture(object sender, byte[] e)
        {
            // TODO change album art
            if (e != null)
            {
                mNowAlbumArt = Bitmap.CreateScaledBitmap(BitmapFactory.DecodeByteArray(e, 0, e.Length), mAlbumArtWidht, mAlbumArtHeight, false);
            }
            else
            {
                mNowAlbumArt = null;
                mAlbumArtView.SetImageBitmap(mDefaultArt);
            }

            if (e.Length == 0)
            {
                Android.Util.Log.Debug("AlbumArt data", "data length is zero");
            }
        }

        private void PlayerFragmentBtn_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;

            if (button != null)
            {
                switch (button.Id)
                {
                    case Resource.Id.moveNextList:
                        
                        break;
                    case Resource.Id.moveNextItem:
                        mParentActivity.Intent.SetAction(MediaService.NEXT_ACTION);
                        break;
                    case Resource.Id.movePastItem:
                        mParentActivity.Intent.SetAction(MediaService.PREV_ACTION);
                        break;
                    case Resource.Id.movePastList:
                        
                        break;
                    case Resource.Id.playBtn:
                        if (!mPlayBtnToggle)
                        {
                            // playBtn.SetBackgroundResource()
                            mParentActivity.Intent.SetAction(MediaService.PAUSE_ACTION);
                            mPlayBtnToggle = true;
#pragma warning disable CS0618 // Type or member is obsolete
                            button.Background = mFragmentView.Resources.GetDrawable(Resource.Drawable.play);
#pragma warning restore CS0618 // Type or member is obsolete
                        }
                        else
                        {
                            if (MediaService.PAUSE_ACTION.Equals(mParentActivity.Intent.Action))
                            {
                                mParentActivity.Intent.SetAction(MediaService.RESUME_ACTION);
                            }
                            else
                            {
                                mParentActivity.Intent.SetAction(MediaService.PLAY_ACTION);
                            }
                            mPlayBtnToggle = false;
#pragma warning disable CS0618 // Type or member is obsolete
                            button.Background = mFragmentView.Resources.GetDrawable(Resource.Drawable.pause);
#pragma warning restore CS0618 // Type or member is obsolete
                            //playBtn.SetBackgroundResource()
                        }
                        break;
                    default:
                        throw new InvalidOperationException("this object undefined");
                }

                // HACK: startId parameter using temporary value
                mServiceProvider.OnStartCommand(mParentActivity.Intent, StartCommandFlags.Redelivery, 10);
            }
        }

        #endregion
        public object GetCheckedList()
        {
            return new NotImplementedException();
        }

        public void InitializeSelectedItem()
        {
            throw new NotImplementedException();
        }

        public void MoveBackParent()
        {
        }

        public void Refresh()
        {
            throw new NotImplementedException();
        }

        public void ResetItemSelectionMode()
        {
        }
    }
}