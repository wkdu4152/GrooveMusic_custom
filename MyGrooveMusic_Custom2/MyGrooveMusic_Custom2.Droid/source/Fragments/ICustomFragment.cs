namespace MyGrooveMusic.Droid.source.Fragments
{
    public interface ICustomFragment
    {
        void MoveBackParent();
        void ResetItemSelectionMode();
        void InitializeSelectedItem();
        void Refresh();
        object GetCheckedList();
    }
}