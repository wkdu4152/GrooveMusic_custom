using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using MyGrooveMusic.container;
using MyGrooveMusic.Droid.source.ItemContainer;
using MyGrooveMusic.Droid.source.ListAdapters;
using MyGrooveMusic.source;
using MyGrooveMusic.source.container;
using MyGrooveMusic.source.service;
using System.Collections;
using System.Collections.Generic;
using System;
using MyGrooveMusic_Custom2.Droid;

namespace MyGrooveMusic.Droid.source.Fragments
{
    enum eAdapterType
    {
        PlayList,
        PlayListItem
    }
    public class PlayListFragment : Fragment, ICustomFragment
    {
        private Android.Views.View mFragment = null;
        private Activity mParentActivity = null;
        private ListView mListView = null;
        private List<OneDriveInfoContainer> mPlayList = new List<OneDriveInfoContainer>();
        private eAdapterType mAdapterType = eAdapterType.PlayList;
        private ChoiceMode mChoiceMode = ChoiceMode.Single;
        private List<PlaylistItem> mItems = null;

        public PlayListFragment(Activity activity)
        {
            mParentActivity = activity;
        }

        public override Android.Views.View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            mFragment = inflater.Inflate(Resource.Layout.FragmentView, container, false);
            mListView = mFragment.FindViewById<ListView>(Resource.Id.folderList);
            mListView.ItemClick += MListView_ItemClick;
            mListView.ItemLongClick += MListView_ItemLongClick;

            renderingItemView(mFragment.Context, ChoiceMode.Single);
            return mFragment;
        }

        private void MListView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            LinearLayout extensionLayout = mParentActivity.FindViewById<LinearLayout>(Resource.Id.longClickBtnContainer);
            if (extensionLayout != null)
            {
                extensionLayout.Visibility = ViewStates.Visible;
                mParentActivity.FindViewById<LinearLayout>(Resource.Id.playlistLongTouchBtns).Visibility = ViewStates.Visible;
            }
            renderingItemView(mFragment.Context, ChoiceMode.Multiple);
        }

        private async void MListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (mChoiceMode == ChoiceMode.Single)
            {
                // read playlists from Onedrive
                if (mAdapterType == eAdapterType.PlayList)
                {
                    mAdapterType = eAdapterType.PlayListItem;

                    string playListName = (string)mListView.Adapter.GetItem(e.Position);

                    // TODO playlist items download
                    ProgressDialog progressDialog = new ProgressDialog(Activity);
                    progressDialog.SetProgressStyle(ProgressDialogStyle.Spinner);
                    progressDialog.SetMessage("playlist download");
                    progressDialog.SetCancelable(false);
                    progressDialog.Show();

                    if (!DataManager.PLAYLIST_METADATA[playListName].IsLoadComplete)
                    {
                        if (!DataManager.PLAYLIST.ContainsKey(playListName))
                        {
                            DataManager.PLAYLIST.Add(playListName, new List<OneDriveInfoContainer>());
                        }
                        else
                        {
                            DataManager.PLAYLIST[playListName].Clear();
                        }

                        DataManager.PLAYLIST[playListName] = await OneDriveService.DownloadPlayListItems(DataManager.PLAYLIST_METADATA[playListName]);
                        DataManager.PLAYLIST_METADATA[playListName].IsLoadComplete = true;
                    }

                    mPlayList = DataManager.PLAYLIST[playListName];
                    progressDialog.Dismiss();

                    renderingItemView(mFragment.Context, ChoiceMode.Single);
                }
                else
                {
                    // TODO play music
                    DataManager.PLAYED_QUEUE = new List<OneDriveInfoContainer>(mPlayList.ToArray());
                    mParentActivity.Intent.SetAction(MediaService.PLAY_ACTION);
                    // HACK: startId parameter using temporary value
                    MediaService.GetInstance().OnStartCommand(mParentActivity.Intent, StartCommandFlags.Redelivery, 10);
                }
            }
            else if (mListView.ChoiceMode == ChoiceMode.Multiple)
            {
                CheckBox checkBox = e.View.FindViewById<CheckBox>(Resource.Id.itemCheckBox);

                if (checkBox.Checked)
                {
                    checkBox.Checked = false;
                    mItems[e.Position].IsSelected = false;
                }
                else
                {
                    checkBox.Checked = true;
                    mItems[e.Position].IsSelected = true;
                }

                PlayListAdapter adapter = mListView.Adapter as PlayListAdapter;
                adapter.SetCheckBox(e.Position);
            }
        }

        private void renderingItemView(Context context, ChoiceMode choiceMode)
        {
            mChoiceMode = choiceMode;
            switch (mAdapterType)
            {
                case eAdapterType.PlayList:
                    mItems = new List<PlaylistItem>();
                    foreach (KeyValuePair<string, PlaylistMeta> pair in DataManager.PLAYLIST_METADATA)
                    {
                        mItems.Add(new PlaylistItem() { PlaylistName = pair.Key, IsSelected = false });
                    }
                    // mListView.Adapter = new ArrayAdapter<string>(context, Android.Resource.Layout.SimpleListItem1, items);
                    mListView.ChoiceMode = choiceMode;
                    mListView.Adapter = new PlayListAdapter(context, mItems);
                    break;
                case eAdapterType.PlayListItem:
                    mListView.ChoiceMode = choiceMode;
                    mListView.Adapter = new CustomAdapter(context, mPlayList);
                    break;
            }
        }

        public void Refresh()
        {
            renderingItemView(mFragment.Context, mChoiceMode);
        }

        public void ResetItemSelectionMode()
        {
            mAdapterType = eAdapterType.PlayList;

            if(mFragment != null)
            {
                renderingItemView(mFragment.Context, ChoiceMode.Single);
            }
        }

        public void MoveBackParent()
        {
            List<PlaylistItem> selectedItems = mItems.FindAll((item) =>
            {
                return item.IsSelected;
            });

            foreach (PlaylistItem item in selectedItems)
            {
                item.IsSelected = false;
            }

            mAdapterType = eAdapterType.PlayList;
            if(mChoiceMode == ChoiceMode.Multiple)
            {
                LinearLayout extensionLayout = mParentActivity.FindViewById<LinearLayout>(Resource.Id.longClickBtnContainer);
                if (extensionLayout != null)
                {
                    extensionLayout.Visibility = ViewStates.Gone;
                    mParentActivity.FindViewById<LinearLayout>(Resource.Id.playlistLongTouchBtns).Visibility = ViewStates.Gone;
                }
            }
            renderingItemView(mFragment.Context, ChoiceMode.Single);
        }

        public void InitializeSelectedItem()
        {
            throw new NotImplementedException();
        }

        public object GetCheckedList()
        {
            return mItems.FindAll((item) =>
            {
                return item.IsSelected;
            }).ToArray();
        }
    }
}