using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using System.IO;

namespace MyGrooveMusic.Droid.source
{
    public struct Configurations
    {
        public string SearchDirectory;
    }

    public class Configure
    {
        private Configurations Configuration;
        public Configure(string path)
        {
            string filePath = string.Format("{0}/{1}", path, "config.json");
            if (File.Exists(filePath))
            {
                using (FileStream configFile = File.OpenRead(filePath))
                {
                    if (configFile.CanRead)
                    {
                        using (StreamReader reader = new StreamReader(configFile))
                        {
                            string configData = reader.ReadLine();
                            Configuration = JsonConvert.DeserializeObject<Configurations>(configData);
                        }
                    }
                }
            }
            else
            {
                // defalut configuration setting
            }
        }

        public void SetSearchDirectory(string path)
        {
            Configuration.SearchDirectory = path;
        }
    }
}