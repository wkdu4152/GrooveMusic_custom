using Android.Content;
using Android.Widget;
using Android.Views;
using System.Collections.Generic;
using MyGrooveMusic.Droid.source.ItemContainer;
using MyGrooveMusic_Custom2.Droid;

namespace MyGrooveMusic.Droid.source.ListAdapters
{
    public class PlayListAdapter : BaseAdapter<string>
    {
        private Context mContext;
        private List<PlaylistItem> mItems;
        private Dictionary<int, bool> mCheckBoxValues;

        private static Android.Graphics.Color TEXT_COLOR = new Android.Graphics.Color(192, 192, 192);
        private static Android.Graphics.Color BACKGROUND_COLOR1 = new Android.Graphics.Color(48, 48, 48);
        private static Android.Graphics.Color BACKGROUND_COLOR2 = new Android.Graphics.Color(64, 64, 64);

        public override int Count
        {
            get
            {
                return mItems.Count;
            }
        }

        public override string this[int position]
        {
            get
            {
                return mItems[position].PlaylistName;
            }
        }

        public PlayListAdapter(Context context, List<PlaylistItem> items) : base()
        {
            mContext = context;
            mItems = items;
            mCheckBoxValues = new Dictionary<int, bool>();
        }

        public override Android.Views.View GetView(int position, Android.Views.View convertView, ViewGroup parent)
        {
            PlayListViewHolder holder = null;
            if (convertView == null)
            {
                convertView = LayoutInflater.From(mContext).Inflate(Resource.Layout.ListView, parent, false);
                convertView.SetBackgroundColor(BACKGROUND_COLOR1);

                TextView label = convertView.FindViewById<TextView>(Resource.Id.itemLabel);
                LinearLayout labelContainer = (LinearLayout)label.Parent;
                // ImageView image = convertView.FindViewById<ImageView>(Resource.Id.itemImage);
                CheckBox checkBox = convertView.FindViewById<CheckBox>(Resource.Id.itemCheckBox);

                holder = new PlayListViewHolder();
                holder.LabelContainer = labelContainer;
                // holder.Image = image;
                holder.CheckBox = checkBox;
                holder.Label = label;

                convertView.Tag = holder;
            }
            else
            {
                holder = convertView.Tag as PlayListViewHolder;
            }

            if (holder != null)
            {
                holder.Label.Text = (string)mItems[position].PlaylistName;

                bool isEvenNum = (position % 2) == 0 ? true : false;
                if (isEvenNum)
                {
                    holder.LabelContainer.SetBackgroundColor(BACKGROUND_COLOR1);
                }
                else
                {
                    holder.LabelContainer.SetBackgroundColor(BACKGROUND_COLOR2);
                }

                ListView listView = parent as ListView;
                holder.CheckBox.Tag = position;
                if (listView.ChoiceMode == ChoiceMode.Multiple)
                {
                    holder.CheckBox.Visibility = ViewStates.Visible;
                }
                else if (listView.ChoiceMode == ChoiceMode.Single)
                {
                    holder.CheckBox.Visibility = ViewStates.Invisible;
                }
                if (mCheckBoxValues.ContainsKey(position))
                {
                    holder.CheckBox.Checked = mCheckBoxValues[position];
                }
                else
                {
                    holder.CheckBox.Checked = false;
                }
            }

            return convertView;
        }

        public void SetCheckBox(int position)
        {
            if (mCheckBoxValues.ContainsKey(position))
            {
                mCheckBoxValues.Remove(position);
            }
            else
            {
                mCheckBoxValues.Add(position, true);
            }
        }

        public override bool IsEnabled(int position)
        {
            return true;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
    }
    public class PlayListViewHolder : Java.Lang.Object
    {
        public LinearLayout LabelContainer { get; set; }
        public ImageView Image { get; set; }
        public TextView Label { get; set; }
        public CheckBox CheckBox { get; set; }
    }
}