using System;

using System.Net;
using System.IO;
using System.Threading.Tasks;
using Android.Util;
using System.Text;

namespace MyGrooveMusic.Droid.source
{
    public class GrooveMusicAuthenticator
    {
        private HttpWebRequest request = null;
        private string mData = null;

        public string AuthenticationCode { get; private set; }

        public GrooveMusicAuthenticator(string url, string app_id, string key)
        {
            mData = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=app.music.xboxlive.com", app_id, key);

            request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Accept = "application/json";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers["Authorization"] = "Bearer " + AuthenticationCode;
        }

        public async Task<string> GetAuthenticationCode()
        {
            try
            {
                UTF8Encoding encoding = new UTF8Encoding();

                byte[] bytes = encoding.GetBytes(mData);
                request.ContentLength = encoding.GetBytes(mData).Length;

                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        AuthenticationCode = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Groove music key request", e.Message);
            }
            finally
            {

            }
            return AuthenticationCode;
        }

        public bool HasAuthenticate()
        {
            return AuthenticationCode != null ? true : false;
        }
    }
}